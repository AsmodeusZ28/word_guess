
mod app;
// When compiling natively:
#[cfg(not(target_arch = "wasm32"))]
fn main() {
	let mut debug = false;
	for arg in std::env::args() {
		if arg == "-d" || arg == "--debug" {debug = true;}
	}

	let native_options = eframe::NativeOptions::default();

	eframe::run_native(
		"Word Guess",
		native_options,
		Box::new( move |cc| {
			Box::new(
				if debug {
					app::WordGuessApp::new(cc).debug()
				} else {
					app::WordGuessApp::new(cc)
				}
			)
	}));
}

// when compiling to web using trunk.
#[cfg(target_arch = "wasm32")]
fn main() {
    // Make sure panics are logged using `console.error`.
    // console_error_panic_hook::set_once();

    // Redirect tracing to console.log and friends:
    // tracing_wasm::set_as_global_default();

    let web_options = eframe::WebOptions::default();
    eframe::start_web(
        "the_canvas_id", // hardcode it
        web_options,
        Box::new(|cc| Box::new(app::WordGuessApp::new(cc))),
    )
    .expect("failed to start eframe");
}