use std::collections::HashMap;
use eframe::egui;
use rand::prelude::*;


pub const WORD_SIZE: usize = 5;
const LETTER_BOX_SIZE: f32 = 35.0;
const REVEAL_ANIM_TIME: f32 = 0.25;
const ERROR_ANIM_TIME: f32 = 0.5;

const BACKGROUND_COLOR: egui::Color32 = hex24_to_color32(0x1E2331);
const BACKGROUND_COLOR_SECONDARY: egui::Color32 = hex24_to_color32(0x23354C);


const TEXT_COLOR: egui::Color32 = hex24_to_color32(0xBBDFCC);

const BOX_COLOR: egui::Color32 = hex24_to_color32(0x6C9BA7);
const CORRECT_COLOR: egui::Color32 = hex24_to_color32(0x3A7D47);
const OUT_OF_PLACE_COLOR: egui::Color32 = hex24_to_color32(0xCEC55E);

type WordResult = [LetterResult; WORD_SIZE];


#[derive(Copy, Clone, Debug)]
pub enum LetterResult {
	Absent,
	OutOfPlace,
	InPlace
}


#[derive(Default)]
enum GameState {
	#[default]
	InProgress,
	Win,
	// Lose,
}


const fn hex24_to_color32(hex: u32) -> egui::Color32 {
	let r = (hex >> 16) as u8;
	let g = (hex >> 8 & 0x0000FF) as u8;
	let b = hex as u8;
	egui::Color32::from_rgb(r, g, b)
}


pub struct WordGuessApp<'a> {
	game_state: GameState,
	target_word: String,
	current_guess: String,
	past_guesses: Vec<(String, WordResult)>,
	wordlist: Vec<&'a str>,
	allowed_guesses: Vec<&'a str>,
	debug: bool,
	guessed_chars: HashMap<char, LetterResult>,
}

impl WordGuessApp<'_> {
	pub fn new(cc: &eframe::CreationContext) -> Self {
		let mut style = (*cc.egui_ctx.style()).clone();
		style.visuals.override_text_color = Some(TEXT_COLOR);
		cc.egui_ctx.set_style(style);


		//process wordlist
		let wordlist = include_str!("wordlist.txt");
		let wordlist = Vec::from_iter(wordlist.lines());

		let allowed_guesses = include_str!("allowed_guesses.txt");
		let allowed_guesses = Vec::from_iter(allowed_guesses.lines());

		let mut app = Self {
			game_state: GameState::InProgress,
			target_word: "Placeholder".to_owned(),
			current_guess: String::new(),
			past_guesses: Vec::new(),
			wordlist,
			allowed_guesses,
			debug: false,
			guessed_chars: HashMap::new(),
		};
		app.generate_new_word();
		app
	}

	fn generate_new_word(&mut self) {
		let mut rng = thread_rng();
		let word_index = rng.gen_range(0..self.wordlist.len());
		self.target_word = self.wordlist[word_index].to_owned();
	}

	#[allow(dead_code)]
	pub fn debug(mut self) -> Self {
		self.debug = true;
		self
	}

	fn restart(&mut self) {
		self.game_state = GameState::InProgress;
		self.current_guess.clear();
		self.past_guesses.clear();
		self.guessed_chars.clear();
		self.generate_new_word();
	}
}

impl eframe::App for WordGuessApp<'_> {
	fn update(&mut self, ctx: & egui::Context, frame: &mut eframe::Frame) {
		let reveal_anim_id = egui::Id::new("reveal_anim_id");
		let error_anim_id = egui::Id::new("error_anim_id");
		let mut enter_pressed = false;

		for event in &ctx.input().events {
			if let egui::Event::Text(s) = event {
				let mut s = s.clone();
				s.retain(|c: char| {
					c.is_ascii_lowercase()
				});
				self.current_guess.push_str(&s);
			}
		}
		self.current_guess.truncate(WORD_SIZE);
		
		// When compiling natively:
		#[cfg(not(target_arch = "wasm32"))]
		if ctx.input().key_pressed(egui::Key::End) {frame.close();}

		if ctx.input().key_pressed(egui::Key::Home) {self.restart();}

		if ctx.input().key_pressed(egui::Key::Backspace) {
			self.current_guess.pop();
		}

		if ctx.input().key_pressed(egui::Key::Enter) {
			enter_pressed = true;
		} 
		
		
		egui::TopBottomPanel::bottom("lower_panel")
				.frame(egui::Frame::none()
					.fill(BACKGROUND_COLOR_SECONDARY)
					.inner_margin(5.0))
				.show(ctx, |ui| {
			ui.with_layout(egui::Layout::top_down(egui::Align::Center), |ui| {
				let qwerty_layout = vec![
					vec!['q','w','e','r','t','y','u','i','o','p'],
					vec!['a','s','d','f','g','h','j','k','l'],
					vec!['z','x','c','v','b','n','m']
					];
					
					let mut keys = Vec::new();
					
					//build keyboard full of results
					for row_letters in qwerty_layout {
						let mut row_keys = Vec::new();
					for letter in row_letters {
						row_keys.push((
							letter,
							self.guessed_chars.get(&letter)
						))
					}
					keys.push(row_keys);
				}
				
				let (click, enter, backspace) = querty_keyboard(ui, keys);
				
				if let Some(c) = click {
					self.current_guess.push_str(&c.to_string());
				}
				enter_pressed |= enter;
				if backspace {self.current_guess.pop();}
			});
		});
		

		if enter_pressed {
			let guess_vec: Vec<char> = self.current_guess.chars().collect();
			
			if validate_input(&guess_vec, &self.wordlist) || validate_input(&guess_vec, &self.allowed_guesses) {
				let results = evaluate_guess(&self.target_word.chars().collect::<Vec<char>>() ,&guess_vec) ;

				for i in 0..results.len() {
					let letter = guess_vec[i];
					let res = results[i];

					match res {
						LetterResult::InPlace => {self.guessed_chars.insert(letter, LetterResult::InPlace);},
						LetterResult::OutOfPlace => {
							if self.guessed_chars.get(&letter).is_none() {
								self.guessed_chars.insert(letter, LetterResult::OutOfPlace);
							}
						},
						LetterResult::Absent => {
							if self.guessed_chars.get(&letter).is_none() {
								self.guessed_chars.insert(letter, LetterResult::Absent);
							}
						},
					}
				}
				
				self.past_guesses.push((self.current_guess.clone(), results));
				self.current_guess.clear();

				//start the reveal animation for the new set of letter boxes
				ctx.clear_animations();
				ctx.animate_bool_with_time(reveal_anim_id, false, REVEAL_ANIM_TIME);

				let mut win = true;
				for result in results {
					match result {
						LetterResult::Absent => win = false,
						LetterResult::OutOfPlace => win = false,
						LetterResult::InPlace => {}
					}
				}
				if win {self.game_state = GameState::Win;}
			} else {
				//start an animation when an invalid word is entered
				ctx.clear_animations();
				ctx.animate_bool_with_time(error_anim_id, false, ERROR_ANIM_TIME);
			}
		}


		egui::CentralPanel::default()
				.frame(egui::Frame::none().fill(BACKGROUND_COLOR))
				.show(ctx, |ui| {
			ui.with_layout(egui::Layout::top_down(egui::Align::Center), |ui| {
				egui::ScrollArea::vertical().show(ui, |ui| {
					ui.heading("Guess the word!");
					if self.debug {ui.heading(self.target_word.to_owned());}
					
					let word_width = LETTER_BOX_SIZE * WORD_SIZE as f32 + ui.spacing().item_spacing.x * (WORD_SIZE as f32 - 1.0);
					for (guess, result) in &self.past_guesses {
						ui.allocate_ui(egui::vec2(word_width,50.0), create_word_boxes(guess, *result, 1.0));
					}

					ui.allocate_space(egui::vec2(word_width,50.0 * 2.0));
					if enter_pressed {
						ui.scroll_to_cursor(Some(egui::Align::BOTTOM));
					}
					ui.allocate_space(egui::vec2(word_width,-50.0 * 2.0));
					
					//if a word has been entered reveal the new letter boxes with an animation //TODO
					let reveal_progress = ctx.animate_bool_with_time(reveal_anim_id, true, REVEAL_ANIM_TIME);
					match self.game_state {
						GameState::InProgress => {
							//if an invalid word has been entered wiggle it
							let wiggle_prog = ctx.animate_bool_with_time(error_anim_id, true, ERROR_ANIM_TIME);
							let wiggle_amount = 25.0 * (1.0-wiggle_prog) * (wiggle_prog * std::f32::consts::TAU * 5.0).sin();
							ui.add_space(wiggle_amount);
							ui.allocate_ui(egui::vec2(word_width,50.0), create_word_boxes(&self.current_guess, [LetterResult::Absent; WORD_SIZE], reveal_progress));
						},
						GameState::Win => {
							let word_width = LETTER_BOX_SIZE * 10.0 + ui.spacing().item_spacing.x * (10.0 - 1.0);
							ui.allocate_ui(egui::vec2(word_width,50.0), create_word_boxes("Congrats!", [LetterResult::Absent; WORD_SIZE], reveal_progress));
						}
					}
				});
			});
		});
	}
}


fn querty_keyboard(ui: &mut egui::Ui, keys: Vec<Vec<(char, Option<&LetterResult>)>>) -> (Option<char>, bool, bool) {
	let mut backspace_pressed = false;
	let mut enter_pressed = false;
	let mut click = None;
	const KEY_SCALE: f32 = 0.75;
	for (i, row) in keys.into_iter().enumerate() {
		let row_width = (LETTER_BOX_SIZE * KEY_SCALE) * (row.len() + 2)as f32 +
				ui.spacing().item_spacing.x * (row.len() - 1) as f32;
		
		ui.allocate_ui(egui::vec2(row_width, 55.0 * KEY_SCALE), |ui| {
			ui.horizontal_top(|ui| {
				for key in row {
					let (letter, res) = key;
					let rt_letter = egui::RichText::new(letter);
	
					let color = match res {
						Some(res) => {
							match res {
								LetterResult::Absent => BOX_COLOR,
								LetterResult::InPlace => CORRECT_COLOR,
								LetterResult::OutOfPlace => OUT_OF_PLACE_COLOR,
							}
						},
						None => {egui::Color32::DARK_GRAY}
					};
					
					let ir = egui::Frame::none()
					.fill(color)
					.rounding(egui::Rounding::same(3.0))
					.outer_margin(3.0)
					.show(ui, |ui| {
						ui.set_width(LETTER_BOX_SIZE  * KEY_SCALE);
						ui.set_height(50.0  * KEY_SCALE);
						ui.centered_and_justified(|ui| {
							ui.heading(rt_letter);
						})
					});

					let response = ir.response.interact(egui::Sense::click());
					if response.clicked() {
						click = Some(letter);
					}
				}

				//add enter key
				if i == 2 {
					let color = egui::Color32::DARK_GRAY;

					let ir = egui::Frame::none()
					.fill(color)
					.rounding(egui::Rounding::same(3.0))
					.outer_margin(3.0)
					.show(ui, |ui| {
						ui.set_width(LETTER_BOX_SIZE  * KEY_SCALE * 2.0);
						ui.set_height(50.0  * KEY_SCALE);
						ui.centered_and_justified(|ui| {
							ui.heading(" ⎆ ");
						})
					});

					let response = ir.response.interact(egui::Sense::click());
					if response.clicked() {
						enter_pressed = true;
					}
				} else if i == 1 {
					let color = egui::Color32::DARK_GRAY;

					let ir = egui::Frame::none()
					.fill(color)
					.rounding(egui::Rounding::same(3.0))
					.outer_margin(3.0)
					.show(ui, |ui| {
						ui.set_width(LETTER_BOX_SIZE  * KEY_SCALE * 2.0);
						ui.set_height(50.0  * KEY_SCALE);
						ui.centered_and_justified(|ui| {
							ui.heading(" 🔙 ");
						})
					});

					let response = ir.response.interact(egui::Sense::click());
					if response.clicked() {
						backspace_pressed = true;
					}
				}

			});
		
		});
	}
	(click, enter_pressed, backspace_pressed)
}


fn create_word_boxes(word: &str, result: WordResult, progress: f32) -> impl FnOnce(&mut egui::Ui) -> egui::InnerResponse<()> {
	let mut chars: Vec<char> = word.to_uppercase().chars().collect();

	if chars.len() < WORD_SIZE {
		chars.resize(WORD_SIZE, ' ');
	}
	
	move |ui: &mut egui::Ui| {
		ui.horizontal_top(|ui| {
			for (i, letter) in chars.iter().enumerate() {
				let color = match result.get(i).get_or_insert(&LetterResult::Absent) {
					LetterResult::Absent => BOX_COLOR,
					LetterResult::InPlace => CORRECT_COLOR,
					LetterResult::OutOfPlace => OUT_OF_PLACE_COLOR,
				};
				
				
				let rt_letter = egui::RichText::new(*letter)
				.size(50.0 * progress)
				.monospace();
				
				egui::Frame::none()
				.fill(color)
				// .inner_margin(3.0)
				.outer_margin(5.0)
				.show(ui, |ui| {
					ui.set_width(LETTER_BOX_SIZE);
					ui.set_height(50.0 * progress);
					ui.centered_and_justified(|ui| {
						ui.heading(rt_letter);
					})
				});
			}
		})
	}
}


pub(crate) fn validate_input(guess_vec: &[char], wordlist: &[&str]) -> bool {
	let mut guess: String = "".to_owned();
	for letter in guess_vec {
		guess += &letter.to_string();
	}
	wordlist.contains(&&guess[..])
}


pub(crate) fn evaluate_guess(target_word_vec: &[char], guess_vec: &[char]) -> WordResult {
	let mut results = [LetterResult::Absent; WORD_SIZE];
	let mut target_word_vec = target_word_vec.to_owned();
	let mut guess_vec = guess_vec.to_owned();

	//eval--check for exact matches
	for i in 0..WORD_SIZE {
		if target_word_vec[i] == guess_vec[i] {
			results[i] = LetterResult::InPlace;
			guess_vec[i] = '0';
			target_word_vec[i] = '1';
		}
	}
	
	//eval--check for out of place matches
	for (i, guess_letter) in guess_vec.iter_mut().enumerate() {
		for target_letter in target_word_vec.iter_mut().take(WORD_SIZE) {
			if *guess_letter == *target_letter {
				results[i] = LetterResult::OutOfPlace;
				*guess_letter = '0';
				*target_letter = '1';
				break
			}
		}
	}
	
	results
}

